﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraFollow : MonoBehaviour {

    [SerializeField]
    float cameraRangeXmin = -15.0f;

    [SerializeField]
    float cameraRangeXmax = 20.0f;

    private Transform target;

    Vector2 cameraExtend;

    void Start()
    {
        Camera cam = GetComponent<Camera>();

        if (cam.orthographic)
        {
            cameraExtend = new Vector2(cam.orthographicSize * Screen.width / Screen.height,
                cam.orthographicSize * Screen.width / Screen.height);
        }
        else
        {
            cameraExtend = Vector2.zero;
        }
    }

	void Update()
    {
        if (target != null)
        {
            if (CanFollow(target.position))
            {
                transform.position = new Vector3(target.position.x, transform.position.y, transform.position.z);
            }
        }
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }

    bool CanFollow(Vector3 position)
    {
        return true;
    }


}
