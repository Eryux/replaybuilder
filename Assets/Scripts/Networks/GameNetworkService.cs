﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

public class GameNetworkService : MonoBehaviour {

    #region UnityCompliant Singleton
    public static GameNetworkService Instance
    {
        get;
        private set;
    }

    public virtual void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            return;
        }

        Destroy(this.gameObject);
    }
    #endregion

    public delegate void NetworkCallbackMessage(byte[] data, int length);

    public delegate void NetworkCallbackConnection(bool status);

    public string nickname;

    TcpClient networkClient;

    Dictionary<byte, NetworkCallbackMessage> networkCallLists;

    byte[] messageToSend;

    bool sendMessage = false;

    bool stopNetwork = false;

    Thread networkThreadReceiver;
    Thread networkThreadSender;

    void Start()
    {
        networkCallLists = new Dictionary<byte, NetworkCallbackMessage>();
        DontDestroyOnLoad(transform.gameObject);
    }

    void RunReceive()
    {
        while (stopNetwork == false)
        {
            byte[] networkBuffer = new byte[1024];

            NetworkStream stream = networkClient.GetStream();
            Int32 bufferLen = stream.Read(networkBuffer, 0, networkBuffer.Length);

            byte msgType = networkBuffer[0];
            if (networkCallLists.ContainsKey(msgType))
            {
                networkCallLists[msgType](networkBuffer, bufferLen);
            }
        }
    }

    void RunSend()
    {
        while (stopNetwork == false)
        {
            if (sendMessage)
            {
                lock (messageToSend)
                {
                    NetworkStream stream = networkClient.GetStream();
                    stream.Write(messageToSend, 0, messageToSend.Length);
                    sendMessage = false;
                }
            }
            else
            {
                Thread.Sleep(100);
            }
        }
    }

    bool StartNetwork()
    {
        if ((networkClient == null && !networkClient.Connected) || (networkThreadReceiver != null && networkThreadReceiver.IsAlive))
            return false;

        stopNetwork = false;

        networkThreadReceiver = new Thread(new ThreadStart(RunReceive));
        networkThreadReceiver.Start();

        networkThreadSender = new Thread(new ThreadStart(RunSend));
        networkThreadSender.Start();

        return true;
    }

    public void Connect(string serverHostname, int serverPort, NetworkCallbackConnection call)
    {
        Debug.Log("Connecting to the server " + serverHostname + " on port " + serverPort);

        try
        {
            networkClient = new TcpClient(serverHostname, serverPort);
            networkClient.NoDelay = true;
        }
        catch (Exception e)
        {
            Debug.Log("Connection failed. " + e.ToString());
            call(false);
            return;
        }

        StartNetwork();
        call(true);
    }

    public void Disconnect()
    {
        if (networkClient == null || !networkClient.Connected)
            return;

        try
        {
            networkClient.Close();
            stopNetwork = true;
            networkCallLists.Clear();
            Debug.Log("Disconnected");
        }
        catch (Exception e)
        {
            Debug.Log("Disconnect failed. " + e.ToString());
        }
    }

    public void SetSocket(TcpClient client)
    {
        networkClient = client;
    }

    public bool IsConnected()
    {
        return (networkClient != null && networkClient.Connected);
    }

    public void SendByteData(byte[] data)
    {
        if (IsConnected())
        {
            messageToSend = data;
            sendMessage = true;
        }
    }

    public void AddCallback(byte type, NetworkCallbackMessage call)
    {
        networkCallLists.Add(type, call);
    }

    public void RemoveCallback(byte type)
    {
        networkCallLists.Remove(type);
    }

}
