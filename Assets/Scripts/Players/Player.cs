﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

[System.Serializable]
public class PlayerState
{

    public PlayerInput input;

    public PlayerStatus status;

    public int networkId;

    public string nickname;

    public PlayerState(int id, string nickname)
    {
        networkId = id;
        this.nickname = nickname;
    }

}

public class Player : MonoBehaviour {

    [SerializeField]
    float movementSpeed;

    [SerializeField]
    float jumpForce;

    [SerializeField]
    int jumpLimit;

    [SerializeField]
    BoxCollider2D groundCollider;

    [SerializeField]
    PlayerFire fireHitbox;

    [Header("Render")]
    [SerializeField]
    Transform renderTransform;

    [SerializeField]
    Animator renderAnimation;

    [SerializeField]
    Text playerNicknameText;

    [Header("Network")]
    [Space(10)]
    public bool hasAuthority;

    [SerializeField]
    float timeBtwPositionSync;

    // ---------------------------

    Rigidbody2D physic;

    PlayerState state;

    float lastPositionSync = 0f;

    int jumpCount = 0;

    bool refreshPosition = false;

    // ---------------------------

    bool isRunningAnim = false;

    bool isJumpingAnim = false;

	void Start ()
    {
        physic = GetComponent<Rigidbody2D>();

        if (state == null)
        {
            CreatePlayerState(-1, "Noryx");
        }

        if (hasAuthority)
        {
            var follow = Camera.main.GetComponent<CameraFollow>();
            follow.SetTarget(transform);
        }
	}
	
	void Update ()
    {
		if (hasAuthority)
        {
            var left = Input.GetButton("Left");
            var right = Input.GetButton("Right");
            var jump = (state.input.jump) ? true : Input.GetButtonDown("Jump");
            var fire = (state.input.fire) ? true : Input.GetButtonDown("Fire");

            bool inputChange = (left != state.input.left || right != state.input.right
                || jump != state.input.jump || fire != state.input.fire);

            state.input.left = left;
            state.input.right = right;
            state.input.jump = jump;
            state.input.fire = fire;

            state.status.x = transform.position.x;
            state.status.y = transform.position.y;

            // Fire
            if (state.input.fire) {
                fireHitbox.Fire();
            }

            // Update data over network
            if (inputChange && lastPositionSync + timeBtwPositionSync < Time.time)
                SyncPlayerState();
            else if (inputChange)
                SyncPlayerInput();
            else if (lastPositionSync + timeBtwPositionSync < Time.time)
                SyncPlayerStatus();
        }

        if (state.status.alive == false)
        {
            gameObject.SetActive(false);
        }
	}

    void FixedUpdate()
    {
        if (refreshPosition)
        {
            physic.MovePosition(new Vector2(state.status.x, state.status.y));
            refreshPosition = false;
        }

        // Movement action
        if (state.input.left)
        {
            physic.velocity = new Vector2(-movementSpeed * Time.deltaTime, physic.velocity.y);
            transform.localScale = new Vector2(-1f, 1f);
            playerNicknameText.gameObject.transform.localScale = new Vector2(-1, 1f);
            RunAnimation();
        }
        else if (state.input.right)
        {
            physic.velocity = new Vector2(movementSpeed * Time.deltaTime, physic.velocity.y);
            transform.localScale = new Vector2(1f, 1f);
            playerNicknameText.gameObject.transform.localScale = new Vector2(1, 1f);
            RunAnimation();
        }
        else
        {
            physic.velocity = new Vector2(0f, physic.velocity.y);
            RunAnimationOff();
        }

        bool isGrounded = IsGrounded();
        if (isGrounded) { jumpCount = 0; JumpAnimationOff(); } else { JumpAnimation(); }
        
        if (state.input.jump && (isGrounded || jumpCount < jumpLimit))
        {
            physic.velocity = new Vector2(physic.velocity.x, jumpForce);
            jumpCount++;
        }

        state.input.jump = false;

        // Fire
        if (state.input.fire)
        {
            FireAnimation();
            state.input.fire = false;
        }
    }

    public bool IsGrounded()
    {
        float bottomBound = groundCollider.bounds.extents.y;
        return Physics2D.Raycast(transform.position - new Vector3(0f, bottomBound + 0.001f, 0f), new Vector2(0f, -1f), bottomBound + 0.001f);
    }

    // Animation -----------------

    void RunAnimation()
    {
        if (isRunningAnim)
            return;

        renderAnimation.SetBool("isRunning", true);
        isRunningAnim = true;
    }

    void RunAnimationOff()
    {
        if (!isRunningAnim)
            return;

        renderAnimation.SetBool("isRunning", false);
        isRunningAnim = false;
    }

    void JumpAnimation()
    {
        if (isJumpingAnim)
            return;

        renderAnimation.SetBool("isJumping", true);
        isJumpingAnim = true;
    }

    void JumpAnimationOff()
    {
        if (!isJumpingAnim)
            return;

        renderAnimation.SetBool("isJumping", false);
        isJumpingAnim = false;
    }

    void FireAnimation()
    {
        renderAnimation.SetTrigger("fire");
    }

    // Network ID ----------------

    public void CreatePlayerState(int networkId, string nickname)
    {
        state = new PlayerState(networkId, nickname);
        state.input = new PlayerInput(false, false, false, false, false);
        state.status = new PlayerStatus(Vector2.zero);

        playerNicknameText.text = nickname;
    }

    public void SetPlayerNetId(int id)
    {
        state.networkId = id;
    }

    public int GetNetworkId()
    {
        return state.networkId;
    }

    public string GetNickname()
    {
        return state.nickname;
    }

    public void SetPlayerNickname(string nickname)
    {
        state.nickname = nickname;
        playerNicknameText.text = nickname;
    }

    // Received and Update -------

    public void RefreshPlayerInput(PlayerInput input)
    {
        state.input.ClonePlayerInput(input);
    }

    public void RefreshPlayerStatus(PlayerStatus status)
    {
        state.status.ClonePlayerStatus(status);
        refreshPosition = true;
    }

    public void RefreshPlayerState(PlayerState s)
    {
        RefreshPlayerInput(s.input);
        RefreshPlayerStatus(s.status);
    }

    public void RefreshPlayerHit()
    {
        state.status.alive = false;
    }

    // Send ----------------------

    public void SyncPlayerInput()
    {
        PlayerNetworkController.Instance.SendPlayerInput(state.input, state.networkId);
    }

    public void SyncPlayerStatus()
    {
        PlayerNetworkController.Instance.SendPlayerStatus(state.status, state.networkId);
        lastPositionSync = Time.time;
    }

    public void SyncPlayerState()
    {
        PlayerNetworkController.Instance.SendPlayerState(state);
        lastPositionSync = Time.time;
    }

    public void SyncPlayerHit(Player target)
    {
        PlayerNetworkController.Instance.SendPlayerHit(state.networkId, target.GetNetworkId());
        Debug.Log(state.nickname + " kill " + target.GetNickname());
    }

}
