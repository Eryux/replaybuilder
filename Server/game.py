import time
import json
import struct
import network
from threading import Thread

class ReplayPlayer():

    def __init__(self, sock, nickname, player_id):
        self.sock = sock

        self.netId = player_id

        self.nickname = nickname
        self.start_position = (0.0, 0.0)
        self.alive = True
        self.score = 0
        self.team = 0
        
    def clone(self, player_id):
        new_player = ReplayPlayer(self.sock, self.nickname, player_id)

        new_player.start_position = (self.start_position[0], self.start_position[1])
        new_player.score = self.score
        new_player.team = self.team
        
        return new_player

    def toDict(self,sock):
        return {
            "networkId": self.netId, 
            "nickname": self.nickname,
            "hasAuthority": (sock == self.sock), 
            "start":self.start_position
        }

class ReplayRecord():

    def __init__(self, msg, at, from_sock):
        self.message = msg
        self.received_at = at
        self.received_from = from_sock

class ReplayRewind():

    def __init__(self, network):
        self.network = network
        self.records = []
        self.play = False

    def add_record(self, message, sock, at):
        self.records.append(ReplayRecord(message, at, sock))

    def play_records(self, at_time):
        if len(self.records) == 0:
            return

        last_record = self.records[0]
        while last_record is not None and last_record.received_at <= at_time:
            # Replay the message and remove it
            self.network.dispatch(last_record.message, None)
            del self.records[0]

            if len(self.records) != 0:
                last_record = self.records[0]
            else:
                last_record = None

class ReplayBuilderLobby():
    
    def __init__(self, listener, config):
        self.lobbies = {}
        self.config = config
        self.network = listener

        listener.add_callback(80, self.find_game)
        listener.add_callback(81, self.create_game)
        listener.add_callback(82, self.join_game)

    def find_game(self, message, sock, service):
        game = None

        for g in self.lobbies:
            if self.lobbies[g].status == "waiting":
                game = self.lobbies[g]
                break
        
        if game is None:
            self.create_game(message, sock, service)
        else:
            print("Player joining game #" + str(game.gameID))
            self.network.remove_client(sock, False)
            game.network.add_client(sock)

    def create_game(self, message, sock, service):
        game_network = network.NetworkListener()
        game_network.sock = self.network.sock
        self.network.remove_client(sock, False)
        game_network.add_client(sock)

        game = ReplayBuilderGame(game_network, self.config)
        game_network.start()

        print("Player joining game #" + str(game.gameID))
        self.lobbies[game.gameID] = game

    def join_game(self, message, sock, service):
        try:
            data = struct.unpack('i', message[1::])
        except struct.error:
            service.send_to_one(sock, bytes([89, 0]))
            return

        if data[0] not in self.lobbies:
            service.send_to_one(sock, bytes([89, 0]))
            return
        
        print("Player joining game #" + str(self.lobbies[data[0]].gameID))
        service.send_to_one(sock, bytes([89, 1]))

        self.network.remove_client(sock, False)
        self.lobbies[data[0]].network.add_client(sock)

class ReplayBuilderGame(Thread):

    gameID_counter = 0

    def __init__(self, network, config):
        Thread.__init__(self)
        self.stop = False

        self.network = network
        self.config = config

        self.recorder = None
        self.play_record = None

        self.players = []
        self.entities = []

        self.status = "waiting"

        self.netID_counter = 0
        self.gameID = ReplayBuilderGame.gameID_counter
        ReplayBuilderGame.gameID_counter += 1

        self.round_counter = 0

        self.round_limit = self.config.getint('ReplayBuilderGame', 'max_round')
        self.score_limit = self.config.getint('ReplayBuilderGame', 'max_score')
        self.timelimit = self.config.getfloat('ReplayBuilderGame', 'timelimit')
        self.player_limit = self.config.getint('ReplayBuilderGame', 'nb_player')

        self.friendlyfire = self.config.getint('ReplayBuilderGame', 'friendly_fire')

        self.start_time = 0
        self.round_start_time = 0

        print("Creating game #" + str(self.gameID))
        self.network.add_callback(51, self.action_add_player)
        self.network.del_client_callback.append(self.action_del_player)

        self.start()

    def run(self):

        # Game loop
        while not self.stop:
            if self.status == "running":
                a_players = self.get_alive_players()
                if len(a_players) < self.player_limit: # One player alive ? Round end
                    self.round_end()
                if time.time() - self.start_time > self.timelimit * 60.0:
                    self.game_end()
            elif self.status == "waiting":
                if len(self.players) == self.player_limit: # Enough player ? Start game
                    self.game_start()

            if self.play_record is not None:
                self.play_record.play_records(time.time() - self.round_start_time)

            time.sleep(0.033)

        for p in self.players:
            self.network.remove_client(p.sock)
                    
    # Game state change
    def game_start(self):
        self.network.remove_callback(51, self.action_add_player)

        if self.round_counter == 0:
            print("Game #" + str(self.gameID) + " start")
            self.start_time = time.time()

            self.network.add_callback(1, self.action_player_data)
            self.network.add_callback(2, self.action_player_data)
            self.network.add_callback(3, self.action_player_data)
            self.network.add_callback(4, self.action_player_hit)

        self.status = "running"
        self.round_start()

    def round_start(self):
        print("Game #" + str(self.gameID) + " new round begin")
        self.round_start_time = time.time()

        # Set replay
        self.play_record = self.recorder
        self.recorder = ReplayRewind(self.network)

        # Duplicate players and remove sock authority (not for first round)
        if self.round_counter != 0:
            nb_players = len(self.players)
            for i in range(0, nb_players):
                new_player = self.players[i].clone(self.netID_counter)
                self.netID_counter += 1

                self.players[i].sock = None
                self.players[i] = new_player
                self.entities.append(new_player)

        for e in self.entities:
            e.alive = True

        # Compiling and send player information for all players
        for p in self.players:
            data = [e.toDict(p.sock) for e in self.entities]

            message = bytearray()
            message.append(31)
            message.extend(json.dumps({"data":data}).encode())

            self.network.send_to_one(p.sock, message)
    
    def round_end(self):
        print("Game #" + str(self.gameID) + " a round end")

        for p in self.players:
            p.alive = True

        self.round_counter += 1
        time.sleep(1)

        # Get max score
        max_score = 0
        for p in self.players:
            if p.score > max_score:
                max_score = p.score

        if self.round_counter >= self.round_limit:
            self.game_end()
        elif max_score >= self.score_limit:
            self.game_end()
        else:
            self.round_start()

    def game_end(self):
        print("Game #" + str(self.gameID) + " end")

        # Send final score
        message = bytearray()
        message.append(35)
        message.extend(struct.pack("iiii", self.players[0].netId, self.players[0].score, self.players[1].netId, self.players[1].score))
        self.network.send_to_many(self.get_players_socket(), message)

        self.network.remove_callback(1, self.action_player_data)
        self.network.remove_callback(2, self.action_player_data)
        self.network.remove_callback(3, self.action_player_data)
        self.network.remove_callback(4, self.action_player_hit)

        self.status = "end"
        self.stop = True

    # Network callback actions
    def action_add_player(self, message, sock, service):
        if self.status != "waiting":
            return

        try:
            player_data = json.loads(message[1::].decode("utf-8"))
        except UnicodeDecodeError:
            return
        except json.JSONDecodeError:
            return

        player = ReplayPlayer(sock, player_data["nickname"], self.netID_counter)

        if len(self.players) == 0:
            player.start_position = (-15.8, -6.8)
            player.team = 0
        elif self.players[0].team == 0:
            player.start_position = (54.0, -6.8)
            player.team = 1
        else:
            player.start_position = (-15.8, -6.8)
            player.team = 0

        self.players.append(player)
        self.entities.append(player)

        self.netID_counter += 1

    def action_del_player(self, sock):
        for p in self.players:
            if p.sock == sock:
                self.players.remove(p)
        
        if len(self.players) < self.player_limit:
            self.network.add_callback(51, self.action_add_player)
            self.status = "waiting"

    def action_player_data(self, message, sock, service):
        self.recorder.add_record(message, sock, time.time() - self.round_start_time)
        self.network.send_to_many(self.get_players_socket([sock]), message)

    def action_player_hit(self, message, sock, service):
        self.recorder.add_record(message, sock, time.time() - self.round_start_time)
        
        try:
            data = struct.unpack('ii', message[1::])
        except struct.error as e:
            return

        attacker = self.get_player_by_id(data[0])
        victim = self.get_player_by_id(data[1])

        if not attacker.alive or not victim.alive:
            return

        if self.friendlyfire == 0 and attacker.team == victim.team:
            return
        
        if attacker is not None:
            if attacker.team == victim.team:
                attacker.score -= 1
            else:
                attacker.score += 1
        
        if victim is not None:
            victim.alive = False

        self.network.send_to_many(self.get_players_socket(), message)
    
    # Utils
    def get_alive_players(self):
        result = [p for p in self.players if p.alive]
        return result

    def get_players_socket(self, avoid_sock = []):
        return [p.sock for p in self.players if p.sock not in avoid_sock]

    def get_player_by_id(self, netID):
        for p in self.entities:
            if p.netId == netID:
                return p
        return None