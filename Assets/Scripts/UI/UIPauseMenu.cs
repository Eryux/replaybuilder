﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class UIPauseMenu : MonoBehaviour {

    [SerializeField]
    CanvasGroup pauseGUI;

    [SerializeField]
    GraphicRaycaster buttonRaycast;

    void Update()
    {
        if (Input.GetButtonDown("Pause"))
        {
            ToggleMenu();
        }
    }

	public void ToggleMenu()
    {
        buttonRaycast.enabled = !buttonRaycast.enabled;
        EventSystem.current.SetSelectedGameObject(null);
        pauseGUI.alpha = (pauseGUI.alpha > 0.5f) ? 0.0f : 1f;
    }

    public void GiveUp()
    {
        GameNetworkService.Instance.Disconnect();
        SceneManager.LoadScene(0);
    }

}
