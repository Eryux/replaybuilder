﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIRandomTip : MonoBehaviour {

    [SerializeField]
    float changeEvery;

    [SerializeField]
    Text txt;

    [SerializeField]
    List<string> tips;

    System.Random rnd;

    float lastChange = 0f;

    void Start()
    {
        rnd = new System.Random();
        txt.text = "Tip : " + tips[rnd.Next(0, tips.Count - 1)];
    }

    void Update()
    {
        if (lastChange + changeEvery < Time.time)
        {
            txt.text = "Tip : " + tips[rnd.Next(0, tips.Count - 1)];
            lastChange = Time.time;
        }
    }


}
