﻿using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PlayerInput
{
    public bool left, right, jump, fire, power;

    public PlayerInput(bool l,bool r,bool j,bool f,bool p)
    {
        left = l;
        right = r;
        jump = j;
        fire = f;
        power = p;
    }

    public void ClonePlayerInput(PlayerInput input)
    {
        left = input.left;
        right = input.right;
        jump = input.jump;
        fire = input.fire;
        power = input.power;
    }

}
