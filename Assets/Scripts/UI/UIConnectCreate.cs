﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIConnectCreate : MonoBehaviour {

    [SerializeField]
    GameConfigReader config;

    [SerializeField]
    InputField nickField;

    [SerializeField]
    Text stateText;

    [SerializeField]
    GameObject sceneElement;

    void Start()
    {
        var cfg = config.GetConfig();
        nickField.text = cfg.nickname;
    }
    
	public void ConnectToServer()
    {
        var cfg = config.GetConfig();

        try
        {
            stateText.text = "Connecting <=======> Server";
            GameNetworkService.Instance.Disconnect();
            GameNetworkService.Instance.Connect(cfg.server_address, cfg.server_port, OnPlayerConnect);
        }
        catch (FormatException e)
        {
            Debug.Log(e.ToString());
            stateText.text = "Incorrect Address :(";
        }
    }

    public void OnPlayerConnect(bool status)
    {
        if (status)
        {
            StartCoroutine(GameSceneLoader(1));
        }
        else
        {
            stateText.text = "Failed to connect :(";
        }
    }

    IEnumerator GameSceneLoader(int scene_id)
    {
        stateText.text = "Creating your game 8D";
        GameNetworkService.Instance.nickname = nickField.text;

        yield return new WaitForSeconds(2);

        byte[] data;
        using (var stream = new MemoryStream())
        {
            stream.WriteByte(81);
            data = stream.ToArray();
        }
        GameNetworkService.Instance.SendByteData(data);

        yield return new WaitForSeconds(2);

        AsyncOperation async = SceneManager.LoadSceneAsync(scene_id, LoadSceneMode.Single);

        while ( ! async.isDone)
        {
            yield return null;
        }
    }

}
