﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFire : MonoBehaviour {

    [SerializeField]
    Player attacker;

    [SerializeField]
    float attackDuration;

    [SerializeField]
    Collider2D HitCollider;

    float attackStart;

    public void Fire()
    {
        attackStart = Time.time;
        HitCollider.enabled = true;
    }

    void Update()
    {
        if (attackStart + attackDuration < Time.time)
        {
            HitCollider.enabled = false;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Player victim = other.GetComponent<Player>();
        if (victim != null && victim != attacker)
        {
            attacker.SyncPlayerHit(victim);
        }
    }

}
