﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIQuit : MonoBehaviour {

	public void QuitGame()
    {
        GameNetworkService.Instance.Disconnect();
        Application.Quit();
    }

}
