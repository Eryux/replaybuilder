﻿[System.Serializable]
public class JsonPlayerData {

    public int networkId;

    public string nickname;

    public bool hasAuthority;

    public float[] start;

}

[System.Serializable]
public class WrapperJsonPlayerData
{
    public JsonPlayerData[] data;
}