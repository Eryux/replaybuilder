﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    #region UnityCompliant Singleton
    public static GameManager Instance
    {
        get;
        private set;
    }

    public virtual void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            return;
        }

        Destroy(this.gameObject);
    }
    #endregion

    [SerializeField]
    CanvasGroup gameEndUI;

    [SerializeField]
    GraphicRaycaster raycaster;

    [SerializeField]
    Text scoreText;

    [SerializeField]
    Text UIPlayerScore1;

    [SerializeField]
    Text UIPlayerScore2;

    bool gameEnd = false;
    bool scoreChange = false;

    int playerId_1;
    int playerId_2;
    int score_1;
    int score_2;

    void Start()
    {
        GameNetworkService.Instance.AddCallback(35, ActionGameEnd);

        score_1 = 0;
        score_2 = 0;
    }

    void Update()
    {
        if (scoreChange)
        {
            UIPlayerScore1.text = score_1.ToString();
            UIPlayerScore2.text = score_2.ToString();
            scoreChange = false;
        }

        if (gameEnd && gameEndUI.alpha < 0.5f)
        {
            if (PlayerNetworkController.Instance.GetPlayerById(playerId_1).hasAuthority)
            {
                if (score_1 > score_2)
                {
                    scoreText.text = "You win - [" + score_1 + " - " + score_2 + "]";
                }
                else if (score_1 < score_2)
                {
                    scoreText.text = "You loose - [" + score_1 + " - " + score_2 + "]";
                }
                else
                {
                    scoreText.text = "Draw - [" + score_1 + " - " + score_2 + "]";
                }
            }
            else
            {
                if (score_1 > score_2)
                {
                    scoreText.text = "You loose - [" + score_1 + " - " + score_2 + "]";
                }
                else if (score_1 < score_2)
                {
                    scoreText.text = "You win - [" + score_1 + " - " + score_2 + "]";
                }
                else
                {
                    scoreText.text = "Draw - [" + score_1 + " - " + score_2 + "]";
                }
            }

            gameEndUI.alpha = 1f;
            raycaster.enabled = true;
        }
    }

	void ActionGameEnd(byte[] data, int len)
    {
        if (len != (1 + 4 * sizeof(int)))
            return;

        playerId_1 = BitConverter.ToInt32(data, 1);
        score_1 = BitConverter.ToInt32(data, 1 + sizeof(int));
        playerId_2 = BitConverter.ToInt32(data, 1 + 2 * sizeof(int));
        score_2 = BitConverter.ToInt32(data, 1 + 3 * sizeof(int));

        GameNetworkService.Instance.Disconnect();

        gameEnd = true;
    }

    public void ScoreRefresh(Player scoredPlayer)
    {
        if (scoredPlayer.hasAuthority)
        {
            score_1++;
        }
        else
        {
            score_2++;
        }

        scoreChange = true;
    }

}
