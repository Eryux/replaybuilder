﻿using System;
using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using UnityEngine;

public class PlayerNetworkController : MonoBehaviour {

    #region UnityCompliant Singleton
    public static PlayerNetworkController Instance
    {
        get;
        private set;
    }

    public virtual void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            return;
        }

        Destroy(this.gameObject);
    }
    #endregion

    [SerializeField]
    GameObject UIWaitingPlayer;

    [SerializeField]
    GameObject playerPrefab;

    GameNetworkService netService;

    Dictionary<int, Player> networkedPlayer;

    BinaryFormatter format;

    JsonPlayerData[] playerToInstance = null;

    void Start()
    {
        format = new BinaryFormatter();
        networkedPlayer = new Dictionary<int, Player>();

        if (netService == null)
        {
            netService = GameNetworkService.Instance;
        }

        netService.AddCallback(1, ActionPlayerStatus);
        netService.AddCallback(2, ActionPlayerInput);
        netService.AddCallback(3, ActionPlayerState);
        netService.AddCallback(4, ActionPlayerHit);

        netService.AddCallback(31, ActionGamePlayer);

        // Add player
        SendAddPlayer(netService.nickname);
    }

    void Update()
    {
        if (playerToInstance != null && playerToInstance.Length > 0)
        {
            foreach (KeyValuePair<int, Player> p in networkedPlayer)
            {
                Destroy(p.Value.gameObject);
            }

            networkedPlayer.Clear();
            GC.Collect();

            for (int i = 0; i < playerToInstance.Length; i++)
            {
                GameObject playerObject = Instantiate(playerPrefab) as GameObject;
                playerObject.transform.position = new Vector2(playerToInstance[i].start[0], playerToInstance[i].start[1]);

                Player playerComponent = playerObject.GetComponent<Player>();
                playerComponent.CreatePlayerState(playerToInstance[i].networkId, playerToInstance[i].nickname);
                playerComponent.hasAuthority = playerToInstance[i].hasAuthority;

                networkedPlayer.Add(playerComponent.GetNetworkId(), playerComponent);
            }

            playerToInstance = null;
            Destroy(UIWaitingPlayer);
        }
    }

    // Received callback ---------

    public void ActionGamePlayer(byte[] data, int len)
    {
        byte[] json_data = new byte[len - 1];
        Array.Copy(data, 1, json_data, 0, len - 1);

        string json_string = Encoding.UTF8.GetString(json_data);

        var players = JsonUtility.FromJson<WrapperJsonPlayerData>(json_string);
        if (players != null)
        {
            playerToInstance = players.data;
        }
    }

    public void ActionPlayerInput(byte[] data, int len)
    {
        int netId = GetPlayerNetId(data, len);
        if (!networkedPlayer.ContainsKey(netId))
            return;

        PlayerInput input = null;

        int utilMessageLength = BitConverter.ToInt32(data, sizeof(int) + 1);

        byte[] utilData = new byte[utilMessageLength];
        Array.Copy(data, 9, utilData, 0, utilMessageLength);

        using (var stream = new MemoryStream(utilData))
        {
            stream.Seek(0, SeekOrigin.Begin);
            input = (PlayerInput) format.Deserialize(stream);
        }

        if (input != null)
        {
            networkedPlayer[netId].RefreshPlayerInput(input);
        }
    }

    public void ActionPlayerStatus(byte[] data, int len)
    {
        int netId = GetPlayerNetId(data, len);
        if (!networkedPlayer.ContainsKey(netId))
            return;
        
        PlayerStatus status = null;
        using (var stream = new MemoryStream(GetGameData(data, len)))
        {
            stream.Seek(0, SeekOrigin.Begin);
            status = (PlayerStatus)format.Deserialize(stream);
        }
        
        if (status != null)
        {
            networkedPlayer[netId].RefreshPlayerStatus(status);
        }
    }

    public void ActionPlayerState(byte[] data, int len)
    {
        int netId = GetPlayerNetId(data, len);
        if (!networkedPlayer.ContainsKey(netId))
            return;

        PlayerState state = null;
        using (var stream = new MemoryStream(GetGameData(data, len)))
        {
            stream.Seek(0, SeekOrigin.Begin);
            state = (PlayerState)format.Deserialize(stream);
        }

        if (state != null)
        {
            networkedPlayer[netId].RefreshPlayerState(state);
        }
    }

    public void ActionPlayerHit(byte[] data, int len)
    {
        if (len != (1 + 2 * sizeof(int)))
            return;

        int attackerId = BitConverter.ToInt32(data, 1);
        int targetId = BitConverter.ToInt32(data, 1 + sizeof(int));

        if (!networkedPlayer.ContainsKey(targetId))
            return;

        networkedPlayer[targetId].RefreshPlayerHit();
        GameManager.Instance.ScoreRefresh(networkedPlayer[attackerId]);
    }

    // Send data -----------------

    public void SendAddPlayer(string nickname)
    {
        JsonPlayerData playerData = new JsonPlayerData();

        playerData.networkId = -1;
        playerData.nickname = nickname;
        playerData.hasAuthority = true;
        playerData.start = new float[2] { 0f, 0f };

        byte[] data;
        using (var stream = new MemoryStream())
        {
            stream.WriteByte(51);

            var encodedPlayerData = Encoding.UTF8.GetBytes(JsonUtility.ToJson(playerData));
            stream.Write(encodedPlayerData, 0, encodedPlayerData.Length);
            data = stream.ToArray();
        }

        netService.SendByteData(data);
    }

    public void SendPlayerInput(PlayerInput input, int netId)
    {
        byte[] data;
        byte[] playerInput;

        using (var stream = new MemoryStream())
        {
            format.Serialize(stream, input);
            playerInput = stream.ToArray();
        }

        using (var stream = new MemoryStream())
        {
            stream.WriteByte(2);

            stream.Write(BitConverter.GetBytes(netId), 0, sizeof(int));
            stream.Write(BitConverter.GetBytes(playerInput.Length), 0, sizeof(int));
            stream.Write(playerInput, 0, playerInput.Length);

            data = stream.ToArray();
        }

        netService.SendByteData(data);
    }

    public void SendPlayerStatus(PlayerStatus status, int netId)
    {
        byte[] data;

        using (var stream = new MemoryStream())
        {
            stream.WriteByte(1);
            stream.Write(BitConverter.GetBytes(netId), 0, sizeof(int));
            format.Serialize(stream, status);
            data = stream.ToArray();
        }

        netService.SendByteData(data);
    }

    public void SendPlayerState(PlayerState state)
    {
        byte[] data;

        using (var stream = new MemoryStream())
        {
            stream.WriteByte(3);
            stream.Write(BitConverter.GetBytes(state.networkId), 0, sizeof(int));
            format.Serialize(stream, state);
            data = stream.ToArray();
        }

        netService.SendByteData(data);
    }

    public void SendPlayerHit(int attackerId, int targetId)
    {
        byte[] data;

        using (var stream = new MemoryStream())
        {
            stream.WriteByte(4);
            stream.Write(BitConverter.GetBytes(attackerId), 0, sizeof(int));
            stream.Write(BitConverter.GetBytes(targetId), 0, sizeof(int));
            data = stream.ToArray();
        }

        netService.SendByteData(data);
    }

    // Utils ---------------------

    int GetPlayerNetId(byte[] data, int len)
    {
        if (len < 5) return -1;
        return BitConverter.ToInt32(data, 1);
    }

    byte[] GetGameData(byte[] data, int len)
    {
        if (len < 4) return null;

        byte[] result = new byte[len - 5];
        Array.Copy(data, 5, result, 0, len - 5);

        return result;
    }

    public Player GetPlayerById(int id)
    {
        return networkedPlayer[id];
    }

}
