﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerStatus
{

    public float x, y;

    public bool alive;

    public PlayerStatus(Vector2 pos)
    {
        x = pos.x;
        y = pos.y;
        alive = true;
    }

    public void ClonePlayerStatus(PlayerStatus status)
    {
        x = status.x;
        y = status.y;
        alive = status.alive;
    }

}