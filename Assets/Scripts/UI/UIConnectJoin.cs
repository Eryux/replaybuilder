﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIConnectJoin : MonoBehaviour {

    [SerializeField]
    GameConfigReader config;

    [SerializeField]
    InputField nickField;

    [SerializeField]
    InputField gameToJoin;

    [SerializeField]
    Text stateText;

    [SerializeField]
    GameObject sceneElement;

    bool updateStateText = false;

    bool joiningGame = false;

    string newStateText;

    void Start()
    {
        var cfg = config.GetConfig();
        nickField.text = cfg.nickname;
    }

    void Update()
    {
        if (updateStateText)
        {
            stateText.text = newStateText;
            updateStateText = false;
        }

        if (joiningGame)
        {
            StartCoroutine(LoadGame(1));
            joiningGame = false;
        }
    }
    
	public void ConnectToServer()
    {
        var cfg = config.GetConfig();

        try
        {
            stateText.text = "Connecting <=======> Server";
            GameNetworkService.Instance.RemoveCallback(89);
            GameNetworkService.Instance.Disconnect();
            GameNetworkService.Instance.Connect(cfg.server_address, cfg.server_port, OnPlayerConnect);
        }
        catch (FormatException e)
        {
            stateText.text = "Incorrect Address :(";
        }
    }

    public void OnPlayerConnect(bool status)
    {
        if (status)
        {
            StartCoroutine(GameSceneLoader());
        }
        else
        {
            stateText.text = "Failed to connect :(";
        }
    }

    public void StatusJoinGame(byte[] data, int len)
    {
        GameNetworkService.Instance.RemoveCallback(89);

        if (len < 2)
        {
            GameNetworkService.Instance.Disconnect();
            newStateText = "Game not found :(";
            updateStateText = true;
        }

        if (data[1] == 1)
        {
            joiningGame = true;
        }
        else
        {
            GameNetworkService.Instance.Disconnect();
            newStateText = "Game not found :(";
            updateStateText = true;
        }
    }

    IEnumerator GameSceneLoader()
    {
        stateText.text = "Joining game ...";

        GameNetworkService.Instance.AddCallback(89, StatusJoinGame);
        GameNetworkService.Instance.nickname = nickField.text;

        int gameJoinId = 0;

        try
        {
            gameJoinId = int.Parse(gameToJoin.text);
        }
        catch (FormatException e)
        {
            stateText.text = "Incorrect game ID";
            GameNetworkService.Instance.Disconnect();
            yield break;
        }

        yield return new WaitForSeconds(2);

        byte[] data;
        using (var stream = new MemoryStream())
        {
            stream.WriteByte(82);
            stream.Write(BitConverter.GetBytes(gameJoinId), 0, sizeof(int));
            data = stream.ToArray();
        }

        GameNetworkService.Instance.SendByteData(data);
    }

    IEnumerator LoadGame(int scene_id)
    {
        yield return new WaitForSeconds(2);
        AsyncOperation async = SceneManager.LoadSceneAsync(scene_id, LoadSceneMode.Single);

        while (!async.isDone)
        {
            yield return null;
        }
    }

}
