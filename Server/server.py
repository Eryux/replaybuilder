import network
import time
import game
import configparser

if __name__ == "__main__":
    config = configparser.ConfigParser()

    try:
        print("Load server settings ...")
        server_config = config.read('config.cfg')
    except IOError:
        print("Can't find configuration file !")
        exit(-1)
    except configparser.ParsingError:
        print("Incorrect configuration file")
        exit(-1)

    default_port = config.getint('Network', 'port')
    print("Server start on port " + str(default_port) + " listen " + config.get('Network', 'host'))
    
    s_listener = network.NetworkListener()
    s_listener.start()

    s_con = network.ConnectionService(config.get('Network', 'host'), default_port, s_listener)
    s_con.start()

    game_lobbies = game.ReplayBuilderLobby(s_listener, config)

    time.sleep(2)

    command = ""
    while command != "quit":
        command = input("")

    s_listener.stop = True
    s_con.stop = True

    time.sleep(2)