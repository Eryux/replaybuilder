import socket
import select
import time
from threading import Thread
				
class NetworkListener(Thread):

    def __init__(self):
        Thread.__init__(self)

        self.sock = None
        self.clients = []

        self.callbacks = {}
        self.add_client_callback = []
        self.del_client_callback = []

        self.delta_time = 1.0 / 60
        self.stop = False

    def add_callback(self, m_type, call_function):
        if m_type not in self.callbacks:
            self.callbacks[m_type] = []
        self.callbacks[m_type].append(call_function)

    def remove_callback(self, m_type, call_function):
        if m_type in self.callbacks:
            if call_function in self.callbacks[m_type]:
                self.callbacks[m_type].remove(call_function)

    def add_client(self, client):
        self.clients.append(client)
        for c in self.add_client_callback:
            c(client)

    def remove_client(self, client, close = True):
        if client not in self.clients:
            return

        for c in self.del_client_callback:
            c(client)

        if close:
            try:
                client.shutdown(socket.SHUT_RDWR)
                client.close()
            except IOError:
                pass

            print("Client disconnected.")

        self.clients.remove(client)

    def remove_all(self):
        for c in self.clients:
            self.remove_client(c)
        
    def run(self):
        print("Listener service start ...")

        while not self.stop:
            start = time.time()

            if self.sock is None or not self.clients:
                time.sleep(1)
                continue

            r_c, w_c, e_c = select.select(self.clients, self.clients, [], 1)

            for reader in r_c:
                try:
                    b_data = reader.recv(1024)
                    if not b_data:
                        self.remove_client(reader)
                    else:
                        self.dispatch(b_data, reader)
                except ConnectionResetError:
                    self.remove_client(reader)
                except ConnectionAbortedError:
                    self.remove_client(reader)
                except IOError:
                    pass

            for error in e_c:
                self.remove_client(error)

            time.sleep(max(self.delta_time - (time.time() - start), 0))

        print("Listener service stoped")

    def dispatch(self, message, sock_from):
        msg_type = message[0]
        if msg_type not in self.callbacks:
            return

        for c in self.callbacks[msg_type]:
            c(message, sock_from, self)

    def send_to_many(self, receivers, message):
        r_c, w_c, e_c = select.select(receivers, receivers, [], 1)

        for writer in w_c:
            try:
                writer.send(message)
            except:
                pass

    def send_to_one(self, receiver, message):
        try:
            receiver.send(message)
        except:
            pass

class ConnectionService(Thread):

    def __init__(self, host, port, listener):
        Thread.__init__(self)

        self.port = port
        self.host = host
        self.listener = listener

        self.sock = None
        self.stop = False

    def connect_client(self, client):
        print("Client connected.")
        self.listener.add_client(client)

    def run(self):
        print("Connection service start ...")
		
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.bind((self.host, int(self.port)))
            self.sock.setblocking(0)
            self.sock.listen(5)

            self.listener.sock = self.sock
        except IOError as e:
            print("Connection service start failed !\n" + e.errno + " : " + e.strerror)
            return
        
        print("Waiting for client")
        while not self.stop:
            try:
                client, address = self.sock.accept()
                self.connect_client(client)
            except IOError:
                pass
            time.sleep(1)

        print("Stopping connection service ...")

    