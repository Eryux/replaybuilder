﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using UnityEngine;

[System.Serializable]
public class JsonGameConfig
{

    public string server_address;

    public int server_port;

    public string nickname;

    public JsonGameConfig()
    {
        server_address = "127.0.0.1";
        server_port = 25030;
        nickname = "Player";
    }

}

public class GameConfigReader : MonoBehaviour {

    JsonGameConfig config;

    void Start()
    {
        if ( ! File.Exists(Application.persistentDataPath + "/config.json"))
        {
            InitializeConfig();
            SaveConfig();
        }

        Debug.Log(Application.persistentDataPath);

	    try {
            using (StreamReader sr = new StreamReader(Application.persistentDataPath + "/config.json"))
            {
                config = JsonUtility.FromJson<JsonGameConfig>(sr.ReadToEnd());
            }
        }
        catch (IOException e) {
            Debug.LogWarning(e.Message);
            InitializeConfig();
        }
        catch (System.IO.IsolatedStorage.IsolatedStorageException e) {
            Debug.LogWarning(e.Message);
            InitializeConfig();
        }
    }

    void SaveConfig()
    {
        try
        {

            FileStream newConfig = File.Create(Application.persistentDataPath + "/config.json");
            byte[] cfgData = new UTF8Encoding(true).GetBytes(JsonUtility.ToJson(config));
            newConfig.Write(cfgData, 0, cfgData.Length);
            newConfig.Close();
        }
        catch (IOException e) { }
    }

    public void InitializeConfig()
    {
        config = new JsonGameConfig();
    }

    public JsonGameConfig GetConfig()
    {
        if (config == null)
        {
            Start();
        }

        return config;
    }

    public void SetNickname(string nickname)
    {
        config.nickname = nickname;
        SaveConfig();
    }

}
